##get feature stack for an image reduce to histogram

import cv2
import numpy as np


class BOW(object):
	def __init__(self,path=None,detector=None,descriptor=None):
		pathCheck = (path != None)
		detdescCheck= (detector!=None and descriptor != None)
		assert (pathCheck or detdescCheck), "no path or params"
		if pathCheck:
			self.load(path)
		else:
			self.init(detector,descriptor)

	def init(self,detector,descriptor):
		self._detector=detector
		self._descriptor=descriptor

	def detector(self):
		import siftfeatures as sf
		return sf.create_detector(self._detector)

	def descriptor(self):
		import siftfeatures as sf
		return sf.create_descriptor(self._descriptor)


	def detect_and_compute(self,img_gray=None,path=None):
		print self,img_gray,path
		assert (img_gray !=None or path !=None),"Give a path or an numpy array"
		import siftfeatures as sf
		img_gray = sf.LoadImage(path) if (img_gray==None) else img_gray
		kps, dscs = sf.detect_and_compute(self.detector(),self.descriptor(),img_gray)
		kp_des=dscs.astype(np.float32)
		kp=np.array([i.pt for i in kps],dtype=np.float32)
		cy,cx=img_gray.shape
		cx/=2
		cy/=2
		return kp,kp_des.astype(np.float32),[cx,cy]
		

	def save(self,path):
		assert (self.eigenvectors!=None and
			self.centers !=None and
			self.mean != None and
			self._detector !=None and
			self._descriptor != None and
			self.maxComponents!=None and
			self.K !=None and
			self.criteria !=None and
			self.attempts != None and
			self.flags != None and
			self.maxSamples != None), "Have you trained yet?"
		import pickle
		fileout=open("%s.model"% (path),"wb")
		pickle.dump(self.__dict__,fileout,2)
		fileout.close()
		print "Success saving model"

	def load(self,path):
		import pickle
		filein=open("%s.model" % (path),"rb")
		data=pickle.load(filein)
		filein.close()
		self.__dict__.update(data) 
		print "Success loading model"

	def train(self,paths,maxComponents=96,
			K=1024,
			criteria=(cv2.TERM_CRITERIA_EPS | cv2.TERM_CRITERIA_MAX_ITER, 1, 10),
			attempts=10,
			flags=cv2.KMEANS_RANDOM_CENTERS,
			maxSamples=None):

		maxSamples=len(paths) if maxSamples==None else maxSamples

		#save parameters
		self.maxComponents=maxComponents
		self.K=K
		self.criteria=criteria
		self.attempts=attempts
		self.flags=flags
		self.maxSamples=maxSamples
	
		import random
		random.shuffle(paths)

		#load detector and descriptor
		det=self.detector()
		desc=self.descriptor()
	
		import siftfeatures as sf
		#get first kp_des
		_,kp_des,_=self.detect_and_compute(path=paths[0])
		#iter over paths and stack features
		kp_des_list=[]
		for i in range(1,maxSamples-1):
			path=paths[i]
			#get features
			_,_kp_des,_=self.detect_and_compute(path=paths[i])
			if _kp_des != None:
				#concatonate to pre existing keypoint descriptions
				kp_des = np.concatenate((kp_des, _kp_des), axis=0)

		#sqrt... useful for pca
		kp_des=np.sqrt(kp_des)

		#now flatten the stack... i.e. make one histogram
		kp_des_mean=np.mean(kp_des, axis=0).reshape(1,-1)

		#now apply PCA to get mean/eigenvectors
		mean, eigenvectors = cv2.PCACompute(kp_des,kp_des_mean)

		#now repeat and only return maxcomponent eigenvectors
		mean, eigenvectors = cv2.PCACompute(kp_des,kp_des_mean,eigenvectors,maxComponents)

		#now project kp_des into new autonormal basis
		projection= cv2.PCAProject(kp_des, mean, eigenvectors)	

		assert K<kp_des.shape[0],"We must have kp_des points than clusters"
		#apply kmeans clustering
		retval, bestLabels, centers = cv2.kmeans(projection, K=K,criteria=criteria, attempts=attempts, flags=flags) 
		self.mean=mean
		self.eigenvectors=eigenvectors
		self.centers=centers
		



def demo(dirpath="dataset/pos"):
	import datasets as ds
	paths=ds.get_paths(dirpath)
	#init bow
	graphicmodel=BOW(detector="FAST",descriptor="SIFT")
	graphicmodel.train(paths)
	graphicmodel.save("MyFirstBOW")
	del graphicmodel
	gm= BOW(path="MyFirstBOW")
	print "MEAN",gm.mean,"CENTERS",gm.centers,"EIGENVECTORS",gm.eigenvectors
