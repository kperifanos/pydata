## Example provided by WIDE IO CONSULTING LTD

import os
import fnmatch
import random
import numpy as np

def get_paths(path):
	import os,fnmatch
	return [os.path.join(dirpath,f) for dirpath,dirnames,files in os.walk(path) for f in fnmatch.filter(files,"*.jpg")]

def generate_sets( train_pos_paths,train_neg_paths,test_pos_paths,test_neg_paths,train_max,test_max):
	print "SANTIY CHECK" , len(train_pos_paths),len(train_neg_paths),len(test_pos_paths),len(test_neg_paths)
	train_pos_path=[]
	for path in train_pos_paths:
		_path=[os.path.join(dirpath,f) for dirpath,dirnames,files in os.walk(path) for f in fnmatch.filter(files,"*.jpg")]
		train_pos_path.extend(_path)
	random.shuffle(train_pos_path)
	train_neg_path=[]
	for path in train_neg_paths:
		_path=[os.path.join(dirpath,f) for dirpath,dirnames,files in os.walk(path) for f in fnmatch.filter(files,"*.jpg")]
		train_neg_path.extend(_path)
	random.shuffle(train_neg_path)
	
	test_pos_path=[]
	for path in test_pos_paths:
		_path=[os.path.join(dirpath,f) for dirpath,dirnames,files in os.walk(path) for f in fnmatch.filter(files,"*.jpg")]
		test_pos_path.extend(_path)
	random.shuffle(test_pos_path)
	test_neg_path=[]
	for path in test_neg_paths:
		_path=[os.path.join(dirpath,f) for dirpath,dirnames,files in os.walk(path) for f in fnmatch.filter(files,"*.jpg")]
		test_neg_path.extend(_path)
	random.shuffle(test_neg_path)

	#subsample
	train_pos_path=train_pos_path[:train_max/2]
	train_neg_path=train_neg_path[:train_max/2]
	test_pos_path=test_pos_path[:test_max/2]
	test_neg_path=test_neg_path[:test_max/2]
	print "TRAIN SET PATH LENGTHS", len(train_pos_path),len(train_neg_path)
	print "TEST SET PATH LENGTHS", len(test_pos_path),len(test_neg_path)
	#create train labels
	#ntrain=len(train_pos_path)+len(train_neg_path)np.array(,dtype=np.float32)
	train_labels=[+1.0 for i in range(len(train_pos_path))]
	train_labels.extend([-1.0 for i in range(len(train_neg_path))])
	train_labels=np.array(train_labels,dtype=np.float32)
	train_pos_path.extend(train_neg_path)
	#create test labels
	#test_labels=[]
	#ntest=len(test_pos_path)+len(test_neg_path)
	#test_labels=np.array([+1.0 for i in range(ntest)],dtype=np.float32)
	test_labels=[+1.0 for i in range(len(test_pos_path))]
	test_labels.extend([-1.0 for i in range(len(test_neg_path))])
	test_labels=np.array(test_labels,dtype=np.float32)
	test_pos_path.extend(test_neg_path)
	print "train_test lengths",len(train_pos_path),len(train_labels),len(test_pos_path),len(test_labels)
	print "labels",test_labels,train_labels
	return train_pos_path,train_labels,test_pos_path,test_labels

def GenerateTestReport( outfilepath,test_set_paths,svm_labels,testing_labels):
	sucrate=float(sum(not not (svm_labels[i]==testing_labels[i])for i in range(len(testing_labels)))*100)/len(testing_labels)
	TP=[]
	TN=[]
	FP=[]
	FN=[]
	#get categories
	for i in range(len(testing_labels)):
		tl,sl=testing_labels[i],svm_labels[i][0]
		#print tl,sl,test_set_paths[i]
		if tl==1 and sl ==1:
			TP.append(test_set_paths[i])
		elif tl==1 and sl==-1:
			FP.append(test_set_paths[i])
		elif tl==-1 and sl == 1:
			FN.append(test_set_paths[i])
		elif tl==-1 and sl == -1:
			TN.append(test_set_paths[i])
		else:
			print "error",tl,sl
	lenTP=len(TP)
	lenFP=len(FP)
	lenFN=len(FN)
	lenTN=len(TN)
	fout=open(outfilepath,"w")
	fout.write("<HEAD><LINK href='../../imgcss.css' rel='stylesheet' type='text/css'></HEAD>")
	fout.write("<h1>Test Report</h1>")
	fout.write("<p>Success rate: %s</p>" % (sucrate))
	fout.write("<h2>True positives(kiss and detect kiss): (%d/%d)</h2>" % (lenTP,lenTP+lenFP))
	for i in TP:
		fout.write("<img src='../../%s' />" % (i)) 
	fout.write("<h2>True negatives(no kiss and detect no kiss): (%d/%d)</h2>" % (lenTN,lenTN+lenFN))
	for i in TN:
		fout.write("<img src='../../%s' />" % (i)) 
	fout.write("<h2>False positives(kiss and detect no kiss): (%d/%d)</h2>" % (lenFP,lenFP+lenTP))
	for i in FP:
		fout.write("<img src='../../%s' />" % (i)) 
	fout.write("<h2>False negatives(no kiss and detect kiss): (%d/%d)</h2>" % (lenFN,lenFN+lenTN))
	for i in FN:
		fout.write("<img src='../../%s' />" % (i)) 
	fout.close()

	#opt directory path 
optPath="opt3.1.2/%d/"
	#create directory (does nothing if already existing)
def CreateDirectory( directory):
	if not os.path.exists(directory):
    		os.makedirs(directory)
	return directory

def CreateCSVTrain( idd,train_labels,train_hists,paths):
	lenTrain=len(train_labels)
	lenHist=len(train_hists[0])
	#open file for writing#
	fout=open( CreateDirectory( optPath % (idd))+"TrainSet.csv","w")
	#write headers
	for i in range(lenHist):
		fout.write("Hist %d," % (i))
	fout.write("Label")
	fout.write("\n")
	for i in range(lenTrain):
		for j in range(lenHist):
			fout.write("%f," % (train_hists[i][j]))
		#write label
		fout.write("%d" % (train_labels[i]))
		fout.write("\n")
		
	fout.close()

def CreateCSVTest( idd,test_labels,test_hists,paths):
	lenTest=len(test_labels)
	lenHist=len(test_hists[0])
	#open file for writing
	#fout=open("/opt/%d/TestSet.csv" % (idd),"w")
	fout=open(CreateDirectory( optPath % (idd))+"TestSet.csv","w")
	#write headers
	
	for i in range(lenHist):
		fout.write("Hist %d," % (i))
	fout.write("Label \n")
	for i in range(lenTest):
		for j in range(lenHist):
			fout.write("%f," % (test_hists[i][j]))
		#write label
		fout.write("%d" % (test_labels[i]))
		fout.write("\n")
	fout.close()

def CreateARFFTrain( idd,train_labels,train_hists):
	lenTrain=len(train_labels)
	lenHist=len(train_hists[0])
	#open file for writing#
	#fout=open("/opt/%d/TrainSet.csv" % (idd),"w")
	fout=open( CreateDirectory( optPath % (idd))+"TrainSet.arff","w")
	#write relation
	fout.write("@RELATION valentine \n")
	#write headers
	for i in range(lenHist):
		fout.write("@ATTRIBUTE hist_%d NUMERIC \n" % (i))
	fout.write("@ATTRIBUTE class {c0,c1} \n")
	fout.write("\n")
	fout.write("@DATA\n")
	for i in range(lenTrain):
		
		for j in range(lenHist):
			fout.write("%f," % (train_hists[i][j]))
		#write label
		if (train_labels[i]) == -1: #no kiss
			fout.write("c0" % (train_labels[i]))
		else:
			fout.write("c1" % (train_labels[i]))
		fout.write("\n")
		
	fout.close()

def CreateARFFTest( idd,test_labels,test_hists):
	lenTest=len(test_labels)
	lenHist=len(test_hists[0])
	#open file for writing
	#fout=open("/opt/%d/TestSet.csv" % (idd),"w")
	fout=open( CreateDirectory( optPath % (idd))+"TestSet.arff","w")
	#write relation
	fout.write("@RELATION valentine \n")
	#write headers
	for i in range(lenHist):
		fout.write("@ATTRIBUTE hist_%d NUMERIC \n" % (i))
	fout.write("@ATTRIBUTE class {c0,c1} \n")
	fout.write("\n")
	fout.write("@DATA\n")
	for i in range(lenTest):
		
		for j in range(lenHist):
			fout.write("%f," % (test_hists[i][j]))
		#write label
		if (test_labels[i]) == -1: #no kiss
			fout.write("c0" % (test_labels[i]))
		else:
			fout.write("c1" % (test_labels[i]))
		fout.write("\n")
	fout.close()


def CreateCSVParams( idd,params):
	fout=open( CreateDirectory( optPath % (idd))+"Params.csv","w")
	last=None
	for i in params:
		fout.write("%s\n" % str(i))
		last=i
	fout.write("%s\n" % str(i.name))
	fout.write("%s\n" % str(i.params))
	fout.close()

