## Exemple provided by WIDE IO CONSULTING LTD

import numpy,scipy,scipy.ndimage,scipy.ndimage.morphology
from matplotlib import pyplot

pi=numpy.pi

def nonmax(im):
  gr=scipy.ndimage.morphology.grey_dilation(im,(3,3))
  return ((gr-im)==0)*im

def harris_smooth(I,alpha=0.04,si=1, imsmooth=scipy.ndimage.gaussian_filter):
    """ Harris Corner Detector """
    if (I.ndim==3):
      I=I.mean(axis=2)
    Ix,Iy = scipy.gradient(I)
    H11 = imsmooth(Ix*Ix, si)
    H12 = imsmooth(Ix*Iy, si)
    H22 = imsmooth(Iy*Iy, si)
    return (H11*H22 - H12**2) - alpha*(H11+H22)**2

def harris_kp(im,nb=100,harris_f=harris_smooth, *args,**kwargs):
   r=nonmax(harris_f(im,*args,**kwargs)).ravel()
   nb=min(nb,(r!=0).sum()) # < don't get more keypoints that their actual number
   ags=scipy.argsort(r)[-nb:]
   return [ (p//im.shape[1],p%im.shape[1]) for p in ags ]

CROSS=[[0,0],[0,1],[0,-1],[1,0],[-1,0]]
def showkkp(im,nb=100, *args,**kwargs):   
   kps=harris_kp(im,nb,*args,**kwargs)
   im=im[:,:,numpy.newaxis].repeat(3,axis=2)
   im/=im.max()
   for p in kps:
     for c in CROSS:
       y,x=p[0]+c[0],p[1]+c[1]       
       im[max(0,y-1):y+1,max(0,x-1):x+1,0]=1
       im[max(0,y-1):y+1,max(0,x-1):x+1,1]=0
       im[max(0,y-1):y+1,max(0,x-1):x+1,2]=0
   pyplot.imshow(im)

# import keypoints.harris as kh

if __name__=="__main__":
  import pylab
  pylab.clf()
  import scipy.misc
  showkkp(scipy.misc.lena()/255.,2000,alpha=0.04,si=8)
  pylab.show()
  



def harris_basic(I):
    """ Harris Corner Detector """
    if (I.ndim==3):
      I=I.mean(axis=2)
    Ix,Iy = scipy.gradient(I)
    H11 = Ix*Ix
    H12 = Ix*Iy
    H22 = Iy*Iy
    return (H11*H22 - H12**2) 
