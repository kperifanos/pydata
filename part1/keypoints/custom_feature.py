## Example provided by WIDE IO CONSULTING LTD
import cv2, numpy, harris
np=numpy

class OpticalFlow:
  def __init__(self,frame1):    
         self.prvs = cv2.cvtColor(frame1,cv2.COLOR_BGR2GRAY)                  
  def process(self,frame2):    
        next = cv2.cvtColor(frame2,cv2.COLOR_BGR2GRAY)
        flow = cv2.calcOpticalFlowFarneback(self.prvs,next, None, 0.5, 3, 15, 3, 5, 1.2, 0)
        self.prvs=next
        return flow        
  def flow2rgb(self,flow):
        hsv = np.zeros((self.prvs.shape[0],self.prvs.shape[1],3),dtype=numpy.uint8); hsv[...,1] = 255
        mag, ang = cv2.cartToPolar(flow[...,0], flow[...,1])       
        hsv[:,:,0] = (ang*180/np.pi/2)[:,:,0]
        hsv[:,:,2] = cv2.normalize(mag,None,0,255,cv2.NORM_MINMAX)
        rgb = cv2.cvtColor(hsv,cv2.COLOR_HSV2BGR)
        return rgb

class MyKeyPointExtractor(object):
  def __init__(self):
    self.of=None
    self.flow=None
  def get_patch(self,I,x,R=16):  
    r=I[max(int(x[0]-R//2),0):int(x[0]-R//2+R),
      max(int(x[1]-R//2),0):int(x[1]-R//2+R),:]
    return r    
  def local_feature(self,patch):
    # return a 1d vector
    return #......
  def detect(self,frame):    
    if (self.of==None):
      # Instantiate optical flow
      self.of=OpticalFlow(frame)
    # push frame to optical flow
    self.flow=self.of.process(frame)
    # concatenate intensity and flow information
    # .....
    # get keypoints (via harris)
    points= #....
    # now compute the points
    descriptors=numpy.vstack([self.local_feature(self.get_patch(r,p)) for p in points ])
    # return the result
    return numpy.array(points),descriptors
    
if __name__=="__main__":
  from vio import player_pil as player
  from vio import display_qt as display  
  p=player.Player("akiyo/i_%04d.jpg")
  d=display.Display()
  kpe=MyKeyPointExtractor()  
  for f in p:
    d.push((f,1))    
    print kpe.detect(f)
    #d.push((kpe.of.flow2rgb(kpe.flow),0))
    