## Example provided by WIDE IO CONSULTING LTD

#feature computer
from bow import BOW
	

class VisionModel(BOW):
	#def __init__(self,params):
		#super(VisionModel,self).__init__(params)

	def detect_and_compute_project(self,img=None,path=None):
		#assert (img!=None or path!=None),"give an image or path"
		import cv2
		import numpy as np
		kp,kp_des,center=self.detect_and_compute(img_gray=img,path=path)
		if kp_des != None:
			kp_des=np.sqrt(kp_des)
			projected= cv2.PCAProject(kp_des, self.mean, self.eigenvectors)
		else:
			kp_des=np.zeros((self.K,),dtype=np.float32)
		return kp,projected,center

	def compute_feature(self,img=None,path=None,metric= 'euclidean',discriminator=100):
		#assert (img!=None or path!=None),"give an image or path"
		import scipy.spatial.distance as spd
		import numpy as np
		#calc and project keypoints
		kp,projected,center=self.detect_and_compute_project(img=img,path=path)
		d=spd.cdist(projected,self.centers,metric)
		#invert
		d=d**-discriminator
		#get rid of infinites
		d[d==np.inf]=1
		#normalise
		normd=np.sum(d,axis=1)
		d=((d.T)/normd).T#
		d=np.array(d,dtype=np.float32)
		return np.average(d,axis=0)	#weights=w,

	def StackFeaturesFromPaths(self,sett,metric= 'euclidean',discriminator=100):
		import numpy as np
		return np.vstack([ self.compute_feature(path=sett[x],metric=metric,discriminator=discriminator) for x in range (len(sett))])


def demo():
	vm= VisionModel("MyFirstBOW")
	return vm.compute_feature(path="dataset/pos/0ab00783cefcdd884ed01356046559c6.jpg")


